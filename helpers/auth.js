// REDIRECT_URIに、DOAPからリダイレクトさせるアプリURIを設定します。
const REDIRECT_URI = "http://10.211.55.2:3000/authorize";

// DOMINO_HOMEに、DOAPをインストールしたDominoのホスト名を設定します。
const DOMINO_HOME = 'http://altair3.local.chiburu.com';

// DOAPに設定したクライアントIDを設定します。
const CLIENT_ID = 'doap_debug';

// DOAPに設定したクライアントシークレットを設定します。
const CLIENT_SECRET = 'doap_secret';


// simple-oauth2のためのコンフィグ
const credentials = {
client: {
    id: CLIENT_ID,
    secret: CLIENT_SECRET
},
auth: {
    tokenHost: DOMINO_HOME,
    authorizePath: 'doap/auth', // DOAPの認可エントリポイント
    tokenPath: 'doap/token' // DOAPのトークンエントリポイント
},
http: {
    rejectUnauthorized: false // DominoのなんちゃってSSLでも通るようにする。
},
options: {
    useBasicAuthorizationHeader: false // Basic認証を使わない(Bearerを使う)。
}};


const oauth2 = require('simple-oauth2').create(credentials);

function getAuthUrl()
{
    const returnVal = oauth2.authorizationCode.authorizeURL(
    {
      scope: 'domino'
    });
    console.log(`Generated auth url: ${returnVal}`);
    return returnVal;
}

// 渡されたトークンをこのアプリではCookieに保存する。
function saveValuesToCookie(token, res) {
    console.log('Start saveValuesToCookie');
    // Parse the identity token
    // const user = jwt.decode(token.token.id_token);
    const user = token.token.user_name;
  
    // Save the access token in a cookie
    res.cookie('OAuth2AccessToken', token.token.access_token, {maxAge: (60 * 60 * 1000), httpOnly: true});
    res.cookie('OAuth2RefreshToken', token.token.refresh_token, {maxAge: (7 * 24 * 60 * 60 * 1000), httpOnly: true});
    // Save the user's name in a cookie
    res.cookie('OAuth2UserName', user, {maxAge: (60 * 60 * 1000), httpOnly: true});
    console.log('End saveValuesToCookie');
}

async function getAccessToken(cookies, res) {
    // Do we have an access token cached?
    let token = cookies.OAuth2AccessToken;

// if (token) {
//   // We have a token, but is it expired?
//   // Expire 5 minutes early to account for clock differences
//   const FIVE_MINUTES = 300000;
//   const expiration = new Date(parseFloat(cookies.graph_token_expires - FIVE_MINUTES));
//   if (expiration > new Date()) {
//     // Token is still good, just return it
//     return token;
//   }
// }

// // Either no token or it's expired, do we have a 
// // refresh token?
// const refresh_token = cookies.graph_refresh_token;
// if (refresh_token) {
//   const newToken = await oauth2.accessToken.create({refresh_token: refresh_token}).refresh();
//   saveValuesToCookie(newToken, res);
//   return newToken.token.access_token;
// }

// // Nothing in the cookies that helps, return empty
// return null;
    return token;
}

async function getTokenFromCode(auth_code, res)
{
    console.log('getTokenFromCode; auth_code', auth_code);
    try {
        let result = await oauth2.authorizationCode.getToken(
        {
            code: auth_code,
            redirect_uri: REDIRECT_URI,
            client_id: credentials.client.id,
            scope: "domino"
        });
        console.log('getTokenFromCode; result', result);

        const token = oauth2.accessToken.create(result);
        console.log('Token created: ', token.token);
    
        saveValuesToCookie(token, res);
        console.log('Saved token');
    
        return token.token.access_token;
    }
    catch (error)
    {
        console.log('Access Token Error', error);
    }
    return "";
}

async function getRefreshToken(cookies, res)
{
    let old_token = cookies.OAuth2AccessToken;
    let refresh_token = cookies.OAuth2RefreshToken;
    console.log('getRefreshToken; refresh_token', refresh_token);
    try {
        const newToken = await oauth2.accessToken.create(
        {
            refresh_token: refresh_token
        }).refresh();
        console.log('getRefreshToken; newToken', newToken);
        saveValuesToCookie(newToken, res);
        return newToken.token.access_token;
    }
    catch (error)
    {
        console.log('Get Refresh Token Error', error.message);
    }
    return "";
}

function clearCookies(res) {
    // Clear cookies
    res.clearCookie('OAuth2AccessToken', {maxAge: 3600000, httpOnly: true});
    res.clearCookie('OAuth2UserName', {maxAge: 3600000, httpOnly: true});
}

exports.getAccessToken = getAccessToken;
exports.clearCookies = clearCookies;
exports.getAuthUrl = getAuthUrl;
exports.getTokenFromCode = getTokenFromCode;
exports.getRefreshToken = getRefreshToken;
exports.DOMINO_HOME = DOMINO_HOME;