// Dominoからデータを取得するURL
const GET_DOMINO_DATA_PATH = '/mail/admin.nsf/api/data/documents';

var express = require('express');
var router = express.Router();
var authHelper = require('../helpers/auth');
var Client = require('node-rest-client-promise').Client;
// var graph = require('@microsoft/microsoft-graph-client');

/* GET /mail */
router.get('/', async function(req, res, next) {
  let parms = { title: 'Inbox', active: { inbox: true } };

  const accessToken = await authHelper.getAccessToken(req.cookies, res);
  const userName = req.cookies.OAuth2UserName;

  if (accessToken && userName) {
    console.log("Access token", accessToken);
    parms.user = userName;

    // Initialize Graph client
    // const client = graph.Client.init({
    //   authProvider: (done) => {
    //     done(null, accessToken);
    //   }
    // });
    let options = {
      connection: {
      'rejectUnauthorized': false,
      'headers': {
        'Authorization': `Bearer ${accessToken}`
      }}
    };
    console.log("Options", options);
    let client = new Client(options);
    client.getPromise(
      authHelper.DOMINO_HOME + GET_DOMINO_DATA_PATH
    ).then(async function(rawData) {
      let resp = rawData.response;
      console.log("Status Code", resp.statusCode);
      if (resp.statusCode == 401)
      {
        const newToken = await authHelper.getRefreshToken(req.cookies, res);
        console.log("New Token", newToken);
        res.redirect('/mail');
      }
      else if (resp.statusCode != 200)
      {
        parms.message = resp.statusMessage;
        parms.error = { status: `${resp.statusCode}: ${resp.statusMessage}` };
        parms.debug = JSON.stringify(rawData.data, null, 2);
        res.render('error', parms);
      }
      else {
        // 取得できたRESTデータの＠マークがビューテンプレート「Handlebars」と相性が悪いため、あらかじめ@を抜く。
        let result = [];
        for (let doc of rawData.data)
        {
          let arr = {
            'doc': {
              "unid": doc['@unid']
              , "modified": doc['@modified']
            }
          };
          result.push(arr);
        }

        parms.messages = result;
        res.render('mail', parms);
      }
    }).catch(function(err) {
      console.log(err);
      parms.message = 'Error retrieving messages';
      parms.error = { status: `${err.code}: ${err.message}` };
      parms.debug = JSON.stringify(err.body, null, 2);
      res.render('error', parms);
    });

  } else {
    // Redirect to home
    res.redirect('/');
  }
});

module.exports = router;